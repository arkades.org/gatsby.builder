# .bash_profile

export GATSBY_DIR=$(dirname $BASH_SOURCE)

type appendpath &>/dev/null \
&& appendpath "$GATSBY_DIR/bin"

true
