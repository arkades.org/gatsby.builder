#!/bin/bash

echo install script:

if [ -f "node_modules.tar.xz" ]; then
  dir-uncompress "node_modules.tar.xz"
else
  ! which yarn &>/dev/null \
  && echo "Could not install package. Install yarn package first." && exit 1

  yarn install \
  || { echo "Some error occured while installing package in '$(pwd)'." && exit 1; }
fi
