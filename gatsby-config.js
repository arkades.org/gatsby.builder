// Plugins and other configurations are defined here


module.exports = {
  siteMetadata: {
    title: 'Amazing Pizza Co | Hendersonville, NC',
  },
  plugins: [
    'gatsby-plugin-react-helmet', 
    `gatsby-plugin-sass`,
    `gatsby-transformer-yaml`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-86931141-1",
        // Puts tracking script in the head instead of the body
        head: false,
        // Setting this parameter is optional
        anonymize: true,
        // Setting this parameter is also optional
        respectDNT: true,
        // Avoids sending pageview hits from custom paths
//         exclude: ["/preview/**", "/do-not-track/me/too/"],
        // Enables Google Optimize using your container Id
//         optimizeId: "YOUR_GOOGLE_OPTIMIZE_TRACKING_ID",
        // Any additional create only fields (optional)
        sampleRate: 5,
        siteSpeedSampleRate: 10,
        cookieDomain: "apcwoodfirepizza.com",
      },
    },
    
//     this is the plugin i was just writing about
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/content`,
        name: "content",
        ignore: [`**/\.*`],
      },
    },
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          // gatsby-remark-relative-images must
          // go before gatsby-remark-images
          "gatsby-remark-component",
          {
            resolve: `gatsby-remark-relative-images`,
          },
          {
            resolve: `gatsby-remark-images`,
            options: {
              // It's important to specify the maxWidth (in pixels) of
              // the content container as this plugin uses this as the
              // base for generating different widths of each image.
              maxWidth: 590,
            },
          },
          {
            resolve: "gatsby-remark-embed-youtube",
            options: {
              width: 800,
              height: 400
            }
          },
          "gatsby-remark-responsive-iframe",
        ]
      }
    },
  ],
}


require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})
