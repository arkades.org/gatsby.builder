FROM alpine:latest

RUN apk update \
    && apk add wget \
    procps \
    shadow \
    bash

COPY . /gatsby
