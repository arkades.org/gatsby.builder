/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// this file is basically the entrance point
// into the building process of the site.

const path = require("path")
const fs = require('fs')
const templateRoot = path.resolve('src/templates')

// check if object is a function

function isFunction(functionToCheck) {
 return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

var walkSync = function(dir, filter = '.*', filelist) {
  var fs = fs || require('fs'),
      files = fs.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function(file) {
    if (fs.statSync(dir + '/' + file).isDirectory()) {
      filelist = walkSync(dir + '/' + file, filter, filelist);
    }
    else {
      filelist.push(dir + '/' + file);
    }
  });
  
  return filelist.filter(function(f) {
    return f.match(`^.*/${filter}$`);
  });
};

var findHooks = function(dir, filter = '.*') {
  var filelist = walkSync(dir, filter);
  var result = []
  for (var i = 0; i < filelist.length; i++) {
    hook = require(filelist[i])
    if (isFunction(hook)) {
      result.push({ hook: hook, hookPath: filelist[i] })
    }
  }
  return result
}

var createNodeFilter = 'createnodes.js';
var createNodeHooks = findHooks(templateRoot, createNodeFilter);

// this hook is executed every time a node is created from a plugin

exports.onCreateNode = ({ node, getNode, actions }) => {
  var basepath

  for (var i = 0; i < createNodeHooks.length; i++) {
    var { hook, hookPath } = createNodeHooks[i]
    basepath = path.relative(templateRoot, path.dirname(hookPath))
    
    hook({ node: node, getNode: getNode, actions: actions, basepath: basepath })
  }
}


var createPagesFilter = 'createpages.js';
var createPagesHooks = findHooks(templateRoot, createPagesFilter);


// Pages are created from here
// I designed it that it searches for 'createpages.js' within
// the templates directory and executes the
// main exported function

exports.createPages = ({ actions, graphql }) => {
  var basepath
 
  for (var i = 0; i < createPagesHooks.length; i++) {
    var { hook, hookPath } = createPagesHooks[i]
    basepath = path.relative(templateRoot, path.dirname(hookPath))
    hook({ actions, graphql, basepath })
  }
}

